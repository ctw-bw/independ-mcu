#include "clutch.h"

// Constructor
Clutch::Clutch(int servo_pin) {

    servo_.attach(servo_pin);

    angle_engaged_ = 30;
    angle_disengaged_ = 150;

    disengage(); // Relax from the start
}

// engage
void Clutch::engage() {
    servo_.write(angle_engaged_);
    is_engaged_ = true;
}

// disengage
void Clutch::disengage() {
    servo_.write(angle_disengaged_);
    is_engaged_ = false;
}

// isEngaged
bool Clutch::isEngaged() {
    return is_engaged_;
}
