#ifndef CLUTCH_H_
#define CLUTCH_H_

#include <Servo.h>


/**
 * @brief Class around the servo-driven ankle clutch
 * 
 */
class Clutch {

public:

    /**
     * @brief Construct a new Clutch object
     * 
     * @param servo_pin PWM pin where servo is connected
     */
    Clutch(int servo_pin);

    /**
     * @brief Engage the clutch
     * 
     */
    void engage();

    /**
     * @brief Disengage the clutch
     * 
     */
    void disengage();

    /**
     * @brief Check if currently engaged
     * 
     * @return bool
     */
    bool isEngaged();

private:

    Servo servo_; ///< Clutch servo object

    bool is_engaged_; ///< Current state

    int angle_engaged_, angle_disengaged_;
};


#endif