#include "main.h"
#include "madgwick-ahrs/basic_linalg.h"

// getLegAngle
float getLegAngle(const float quat[4]) {
    float v[3] = {0.0, 0.0, -1.0}; // Get angle between vector pointing
    // down and the same vector but rotated

    float quat_conj[4];
    quaternionConjugate(quat, quat_conj);
    
    rotateVectorByQuaternion(v, quat_conj);

    return -atan2f(v[1], v[0]) * RAD_TO_DEG;
}

// getAnkleAngle
float getAnkleAngle(const float quat_leg_to_world[4], const float quat_foot_to_world[4]) {
   
    float quat_world_to_leg[4];
    quaternionConjugate(quat_leg_to_world, quat_world_to_leg);

    float quat_foot_to_leg[4]; // Rotate leg frame to foot frame
    quaternionMultiply(quat_foot_to_world, quat_world_to_leg, quat_foot_to_leg);

    float v[3] = {0.0, 1.0f, 0.0f}; // Test vector in leg frame

    rotateVectorByQuaternion(v, quat_foot_to_leg);

    return atan2f(v[2], v[1]) * RAD_TO_DEG; // Angle along +x 
    // (positive for toe lift)
}

// getAnkleVelocity
float getAnkleVelocity(const float quat_leg[4], const float quat_foot[4],
                const float gyro_leg[3], const float gyro_foot[3]) {
    
    float quat_world_to_leg[4];
    quaternionConjugate(quat_leg, quat_world_to_leg);

    float quat_foot_to_leg[4]; // Rotate leg frame to foot frame
    quaternionMultiply(quat_foot, quat_world_to_leg, quat_foot_to_leg);

    float gyro_foot_in_leg_frame[3];
    rotateVectorByQuaternion(gyro_foot, quat_foot_to_leg, gyro_foot_in_leg_frame);

    return gyro_foot_in_leg_frame[0] - gyro_leg[0]; // Velocity around +x
    // in leg frame
}

// serialWrite
void serialWrite(const byte* data, size_t size) {
    Serial.write(data, size);
}
