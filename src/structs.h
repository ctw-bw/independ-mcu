#ifndef STRUCTS_H_
#define STRUCTS_H_

#include "duct-mcu/duct.h"


/**
 * @brief Struct to keep track of basic IMU data (raw)
 * 
 */
struct IMUStruct : public DuctStruct {

    float acc[3];
    float gyr[3];
};

/**
 * @brief Struct to track processing results of the gait
 * 
 */
struct GaitStruct : public DuctStruct {

    float hip_flexion;
    float ankle_angle;
    float ankle_velocity;
};

/**
 * @brief Struct containing settings for the MCU program
 * 
 */
struct OptionsStruct : public DuctStruct {

    float test;
};


#endif
