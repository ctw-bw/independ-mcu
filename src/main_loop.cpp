#include "main.h"
#include "madgwick-ahrs/basic_linalg.h"


#define ENABLE_SCOPE    true

/**
 * @brief Remove vertical rotation from a quaternion
 * 
 * @param q 
 */
void remove_yaw_from_quaternion(float q_frame_to_world[4]) {

    float quat_world_to_frame[4];
    quaternionConjugate(q_frame_to_world, quat_world_to_frame);

    const float v_frame[3] = {0.0, 1.0f, 0.0f}; // Test vector in frame
    float v_world[3];

    rotateVectorByQuaternion(v_frame, q_frame_to_world, v_world); // Vector now represents the 
    // frame, expressed in the world frame

    float v_world_plane[3] = {0.0f, v_world[1], v_world[2]};
    // Zero out any rotations around the leg +z

    float quat_correction[4]; // Quaternion to bring the foot frame back into
    // the vertical plane spanned by the leg frame
    quaternionFromVectors(v_world, v_world_plane, quat_correction);

    // Now correct the foot orientation with this offset
    float q_corrected[4];
    quaternionMultiply(quat_correction, q_frame_to_world, q_corrected);

    memcpy(q_frame_to_world, q_corrected, sizeof(float) * 4);
}

void main_loop() {

    for (size_t i = 0; i < 2; i++) {

        if (imus[i]->gyroAvailable()) {
            imus[i]->readGyro();
        }
        if (imus[i]->accelAvailable()) {
            imus[i]->readAccel();
        }
    }

    ahrs_foot.updateIMU(imu_foot.g, imu_foot.a);
    ahrs_leg.updateIMU(imu_leg.g, imu_leg.a);

    static bool calibrate = false; // True while re-calibrating
    static bool button_calibrate = false; // True when triggered through button

    static IMUCalibrator calibrators[] = {
        IMUCalibrator(imus[0]), IMUCalibrator(imus[1])
    };

    if (calibrate) {

        // millis() of the start
        static unsigned long start_millis = 0;
        static bool started_with_button = false;

        if (start_millis == 0) {
            start_millis = millis();

            imus[0]->setRotationOffset(); // Clear rot matrix
            imus[1]->setRotationOffset();

            started_with_button = button_calibrate;

            PRINTLN("Starting calibration...");
        }

        if (millis() - start_millis < 1000) {

            calibrators[0].calibrateGravity();
            calibrators[1].calibrateGravity();

        } else if (millis() - start_millis < 5000) {

            // calibrators[0].calibrateForward();
            // calibrators[1].calibrateForward();
            calibrators[0].calibrateBend();
            calibrators[1].calibrateBend();

        }
        if (millis() - start_millis >= 5000 ||
                started_with_button && !button_calibrate) {
                    
            calibrators[0].complete();
            calibrators[1].complete();

            ahrs_leg.quaternionInitIMU(imu_leg.a);
            ahrs_foot.quaternionInitIMU(imu_foot.a);

            start_millis = 0;
            calibrate = false;

            PRINTLN("Finished recalibrating");
        }
    }


    // User input
    if (Serial1.available()) {
        char c = Serial1.read();

        switch (c) {

        case 'c':
            calibrate = true;
            break;
        case 'r':
            imu_foot.setRotationOffset();
            imu_leg.setRotationOffset();
        }
    }

    static unsigned int calibrate_button_debounce = 0;
    if (digitalRead(pin_button) == LOW) { // If button pressed (flipped, because PULL_UP)
        if (calibrate_button_debounce < 10) {
            calibrate_button_debounce++;
        }
    } else {
        if (calibrate_button_debounce > 0) {
            calibrate_button_debounce--;
        }
    }

    if (calibrate_button_debounce >= 10 && !button_calibrate) {
        calibrate = true;
        button_calibrate = true;
    }
    if (calibrate_button_debounce == 0) {
        button_calibrate = false;
    }

    remove_yaw_from_quaternion(ahrs_leg.quaternion());
    remove_yaw_from_quaternion(ahrs_foot.quaternion());

    struct_gait.ankle_angle = getAnkleAngle(ahrs_leg.getQuaternion(),
                                            ahrs_foot.getQuaternion());


    // Send all IMU data over but slowly
    // static size_t send_counter = 0;
    // if (send_counter++ >= 10) {

    //     duct.send_by_addr(struct_gait);

    //     // memcpy(struct_imu.acc, imu_leg.a, sizeof(float) * 3);
    //     // memcpy(struct_imu.gyr, imu_leg.g, sizeof(float) * 3);
    //     // duct.send_by_addr(struct_imu);

    //     send_counter = 0;
    // }

    // Duct receive
    // size_t bytes_available = Serial.available();
    // if (bytes_available > 0) {

    //     uint8_t buffer[DUCT_BUFFER_SIZE]; // Serial buffer is max 64 bytes anyway
    //     size_t bytes_read = Serial.readBytes(buffer, bytes_available);

    //     if (bytes_read > 0) {
    //         duct.process_bytes(buffer, bytes_read);
    //     }
    // }
    
#if ENABLE_SCOPE
    static Scope scope(8);

    size_t scope_i = 0;

    // scope.set(scope_i++, struct_gait.ankle_angle);
    // scope.set(scope_i++, struct_gait.ankle_velocity);

    // scope.set(imu_foot.a, scope_i, 3); scope_i += 3;
    // scope.set(imu_leg.a, scope_i, 3); scope_i += 3;

    // scope.set(imu_foot.g, scope_i, 3); scope_i += 3;
    // scope.set(imu_leg.g, scope_i, 3); scope_i += 3;

    scope.set(ahrs_leg.getQuaternion(), scope_i, 4); scope_i += 4;
    scope.set(ahrs_foot.getQuaternion(), scope_i, 4); scope_i += 4;

    scope.send();


    // static Scope scope1(1, static_cast<Stream*>(&Serial1));

    // scope1.set(0, struct_gait.ankle_angle);
    // scope1.send();

#endif

}
