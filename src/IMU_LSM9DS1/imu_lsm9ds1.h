#ifndef IMU_LSM9DS1_H_
#define IMU_LSM9DS1_H_

#include <SparkFunLSM9DS1.h>

// Set to false to disable keeping the computed values inside the object
#ifndef BUFFER_FLOATS
#define BUFFER_FLOATS true
#endif

#ifndef ROTATE_FRAME
#define ROTATE_FRAME true
#endif

/**
 * Advanced class for LSM9DS1 sensor.
 * 
 * Largely based on the Sparkfun LSM9DS1 library. By extending
 * the class we can add our own features and modify some of the 
 * existing ones.
 * 
 * WARNING: This extension changes the axis! Do not use the buffered
 * values unless you want to use the original axes.
 */
class IMU_LSM9DS1 : public LSM9DS1 {

public:

    /** Constructor */
    IMU_LSM9DS1();

    /**
     * Query sensor over I2C and store buffered values.
     * 
     * If BUFFER_FLOATS == true, float values will also be computed 
     * immediately.
     */
    void readGyro();

    /**
     * Query sensor over I2C and store buffered values.
     * 
     * If BUFFER_FLOATS == true, float values will also be computed 
     * immediately.
     */
    void readAccel();

    /**
     * Query sensor over I2C and store buffered values.
     * 
     * If BUFFER_FLOATS == true, float values will also be computed 
     * immediately.
     */
    void readMag();

    /**
     * @brief Get (converted) gyroscope output
     * 
     * @param g         Destination
     * @param safe      If true (default), the index in destination is not updated
     *                  in case of an obvious I2C error.
     */
    void getGyro(float g[3], bool safe = true);

    /** Get (converted) accelerometer output */
    void getAccel(float a[3], bool safe = true);

    /** Get (converted) magnetometer output */
    void getMag(float m[3], bool safe = true);

#if BUFFER_FLOATS
    float g[3], a[3], m[3]; ///< Last measured and computed readouts
#endif

#if ROTATE_FRAME

    /**
     * @brief Set the Rotation Offset object
     * 
     * @param rot_mat Rotation matrix to go from sensor frame to world frame
     */
    void setRotationOffset(const float rot_mat[9]);
    void setRotationOffset();

    /**
     * @brief Rotate vector in place by internal rotation offset
     * 
     * @param[in,out] vec 
     */
    void rotateVector(float vec[3]);

#endif

private:

    /**
     * @brief Assign new value only if it does not seem glitched.
     * 
     * @param[in,out] destination 
     * @param new_value
     * @param filter        Set to false to skip filter
     * @return bool         False when no new value was assigned
     */
    bool safeAssign(float& old_value, float new_value, bool filter = true);

#if ROTATE_FRAME

    float rot_offset[9]; ///< Rotation matrix for frame offset (row-major)
    bool has_rot_offset; ///< True of rotation matrix was set

#endif

};

#endif
