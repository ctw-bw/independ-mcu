#include "imu_lsm9ds1.h"

#define X 0
#define Y 1
#define Z 2

// Constructor
IMU_LSM9DS1::IMU_LSM9DS1() : LSM9DS1() { 
#if ROTATE_FRAME
    has_rot_offset = false;
#endif
}

// readGyro
void IMU_LSM9DS1::readGyro() {
    LSM9DS1::readGyro(); // Let parent method handle the I2C stuff

#if BUFFER_FLOATS
    getGyro(g);
#endif
}

// readAccel
void IMU_LSM9DS1::readAccel() {
    LSM9DS1::readAccel(); // Let parent method handle the I2C stuff

#if BUFFER_FLOATS
    getAccel(a);
#endif
}

// readMag
void IMU_LSM9DS1::readMag() {
    LSM9DS1::readMag(); // Let parent method handle the I2C stuff

#if BUFFER_FLOATS
    getMag(m);
#endif
}

// getGyro
void IMU_LSM9DS1::getGyro(float g_buf[3], bool safe) {

    // In case of I2C errors, the values will be ridiculous.
    // When this happens, just ignore the new value and continue with
    // the old ones.

    safeAssign(g_buf[X], calcGyro(gx), safe);
    safeAssign(g_buf[Y], calcGyro(gy), safe);
    safeAssign(g_buf[Z], -calcGyro(gz), safe);

#if ROTATE_FRAME
    rotateVector(g_buf);
#endif
}

// getAccel
void IMU_LSM9DS1::getAccel(float a_buf[3], bool safe) {

    // See getGyro

    safeAssign(a_buf[X], calcAccel(ax), safe);
    safeAssign(a_buf[Y], calcAccel(ay), safe);
    safeAssign(a_buf[Z], -calcAccel(az), safe);

#if ROTATE_FRAME
    rotateVector(a_buf);
#endif
}

// getMag
void IMU_LSM9DS1::getMag(float m_buf[3], bool safe) {

    // See getGyro

    // Pre-determined offset due to hard iron effects
    // Order is in _sensor_ xyz, not body xyz (so pre- rotation correction)
    const float offset[3] = {-0.0044f, 0.2532f, -0.0071};

    safeAssign(m_buf[X], calcMag(mx) - offset[0], safe);
    safeAssign(m_buf[Y], -calcMag(my) + offset[1], safe);
    safeAssign(m_buf[Z], calcMag(mz) - offset[2], safe);

#if ROTATE_FRAME
    rotateVector(m_buf);
#endif
}

// safeAssign
bool IMU_LSM9DS1::safeAssign(float& old_value, float new_value, bool filter) {
    
    if (!filter || abs(new_value < 1.0e5)) {
        old_value = new_value;
        return true;
    }

    return false;
}

#if ROTATE_FRAME

// setRotationOffset
void IMU_LSM9DS1::setRotationOffset(const float rot_mat[9]) {

    memcpy(rot_offset, rot_mat, 9 * sizeof(float));
    has_rot_offset = true;
}
void IMU_LSM9DS1::setRotationOffset() {
    has_rot_offset = false;
}

// rotateVector
void IMU_LSM9DS1::rotateVector(float vec[3]) {

    if (!has_rot_offset) {
        return; // Do nothing
    }

    float y[3];

    y[0] = rot_offset[0] * vec[0] + rot_offset[1] * vec[1] + rot_offset[2] * vec[2];
    y[1] = rot_offset[3] * vec[0] + rot_offset[4] * vec[1] + rot_offset[5] * vec[2];
    y[2] = rot_offset[6] * vec[0] + rot_offset[7] * vec[1] + rot_offset[8] * vec[2];

    memcpy(vec, y, sizeof(float) * 3);
}

#endif
