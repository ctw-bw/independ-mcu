#include "imu_lsm9ds1.h"

/**
 * @brief Custom calibration for an IMU
 * 
 * Calibration is based on:
 *  - Gravity (while not moving)
 *  AND
 *      - A forward movement (looking for +y)
 *      OR
 *      - A knee bend (looking for -x)
 * 
 */
class IMUCalibrator {

public:

    /**
     * @brief Construct a new IMUWithCalibration object
     * 
     * @param imu   Pointer to IMU instance
     */
    IMUCalibrator(IMU_LSM9DS1* imu_ptr);

    /**
     * @brief Perform gravity calibration (call repeatedly)
     */
    void calibrateGravity();

    /**
     * @brief Perform calibration by moving forward (call repeatedly)
     */
    void calibrateForward();

    /**
     * @brief Perform calibration by bending the knee (call repeatedly)
     */
    void calibrateBend();

    /**
     * @brief Finish calibration and write to IMU instance.
     */
    void complete();

private:

    enum State {
        IDLE = 0,
        CALIBRATE_GRAVITY,
        CALIBRATE_FORWARD,
        CALIBRATE_BEND
    };

    void setState(State new_state);

    IMU_LSM9DS1* imu;
    State state;
    float vec_x[3], vec_y[3], vec_z[3];
    float g[3], vel[3];
};
