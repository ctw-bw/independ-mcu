#include "imu_calibrator.h"
#include "../madgwick-ahrs/basic_linalg.h"
#include "../debug.h"

// Create shortcut for i looping from 0 to 2
#define for_i for (size_t i = 0; i < 3; i++)

// Constructor
IMUCalibrator::IMUCalibrator(IMU_LSM9DS1* imu_ptr) {

    imu = imu_ptr;
    state = IDLE;
}

// calibrateGravity
void IMUCalibrator::calibrateGravity() {

    static unsigned int gravity_counter = 0;

    // Initialize
    if (state != CALIBRATE_GRAVITY) {
        for_i {
            g[i] = 0.0f;
        }
        gravity_counter = 0;
    }

    setState(CALIBRATE_GRAVITY);

    // Produce a running average 
    for_i {
        g[i] = (g[i] * gravity_counter + imu->a[i]) / (gravity_counter + 1);
    }

    gravity_counter++;
}

// calibrateForward
void IMUCalibrator::calibrateForward() {

    // Initialize
    if (state != CALIBRATE_FORWARD) {
        for_i {
            vec_y[i] = 0.0f;
            vel[i] = 0.0f; // Velocity, integrated from acceleration
        }
    }

    setState(CALIBRATE_FORWARD);

    // Integrate acceleration to position (minus gravity)
    for_i {
        vel[i] += imu->a[i] - g[i];
        vec_y[i] += vel[i];
        // We ignore the timestap, because we will normalize this anyway
    }
}

// calibrateBend
void IMUCalibrator::calibrateBend() {

    // Initialize
    if (state != CALIBRATE_BEND) {
        for_i {
            vec_x[i] = 0.0f;
        }
    }

    setState(CALIBRATE_BEND);

    for_i {
        vec_x[i] += -1.0f * imu->g[i]; // Integrate negative of gyroscope to find +x
    }
}

// setState
void IMUCalibrator::setState(State new_state) {

    if (state == new_state) {
        return;
    }

    // Deinit states
    switch (state) {
    case CALIBRATE_GRAVITY:
        normalize(g, vec_z);
        PRINT("g: ");
        PRINT(g[0]); PRINT(", ");
        PRINT(g[1]); PRINT(", ");
        PRINTLN(g[2]);
        break;

    case CALIBRATE_FORWARD:

        PRINT("vec_y: ");
        PRINT(vec_y[0]); PRINT(", ");
        PRINT(vec_y[1]); PRINT(", ");
        PRINTLN(vec_y[2]);

        normalize(vec_y); // Normalize in-place
        break;

    case CALIBRATE_BEND:

        PRINT("vec_x: ");
        PRINT(vec_x[0]); PRINT(", ");
        PRINT(vec_x[1]); PRINT(", ");
        PRINTLN(vec_x[2]);

        normalize(vec_x); // Normalize in-place
        break;
    }

    state = new_state;
}

// complete
void IMUCalibrator::complete() {

    State last_state = state;

    setState(IDLE); // Trigger exit actions

    if (last_state == CALIBRATE_FORWARD) {
        // We already have a rough +z and +y

        // Compute +x with the cross product:
        cross(vec_y, vec_z, vec_x);

        // Now compute +y again so it's perpendicular to +z
        cross(vec_z, vec_x, vec_y);

        PRINT("vec_x: ");
        PRINT(vec_x[0]); PRINT(", ");
        PRINT(vec_x[1]); PRINT(", ");
        PRINTLN(vec_x[2]);

    } else if (last_state == CALIBRATE_BEND) {
        // We already have a rough +z and +x

        // Compute +y with the cross product:
        cross(vec_z, vec_x, vec_y);

        // Now compute +x again so it's perpendicular to +z
        cross(vec_y, vec_z, vec_x);

        PRINT("vec_y: ");
        PRINT(vec_y[0]); PRINT(", ");
        PRINT(vec_y[1]); PRINT(", ");
        PRINTLN(vec_y[2]);
    }

    float dot_check = fabs(dot(vec_x, vec_y)) + fabs(dot(vec_x, vec_z))
                                     + fabs(dot(vec_y, vec_z));
                
    if (dot_check > 1e-6f) {
        PRINTLN("Error! Unit vectors not perpendicular");
        PRINT("dot_check: "); PRINTLN(dot_check * 1e7f);
    }

    // Normalize, just to make sure
    normalize(vec_x);
    normalize(vec_y);
    normalize(vec_z);

    // Now +x, +y and +z make up the columns of rotation matrix R' (transpose)
    float R[9] = {vec_x[0], vec_x[1], vec_x[2],
                  vec_y[0], vec_y[1], vec_y[2],
                  vec_z[0], vec_z[1], vec_z[2],
                };

    imu->setRotationOffset(R);
}