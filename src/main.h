#ifndef MAIN_H_
#define MAIN_H_

#include "IMU_LSM9DS1/imu_lsm9ds1.h"
#include "IMU_LSM9DS1/imu_calibrator.h"
#include "madgwick-ahrs/madgwick.h"
#include "uScope-Probe/scope.h"
#include "duct-mcu/duct.h"
#include "structs.h"
#include "clutch/clutch.h"
#include "debug.h"

// -------- Global Variables --------

extern const unsigned long TS;

extern IMU_LSM9DS1 imu_foot, imu_leg;
extern IMU_LSM9DS1* imus[2];

extern Madgwick ahrs_foot, ahrs_leg;

extern Duct duct;

extern IMUStruct struct_imu;
extern GaitStruct struct_gait;

extern const int pin_button;

// -------- Global Functions --------

/**
 * @brief Timed loop function.
 */
void main_loop();

/**
 * @brief Get leg angle from leg-IMU quaternion.
 * 
 * @param quat      Quaternion
 * @return float    Leg angle
 */
float getLegAngle(const float quat[4]);

/**
 * @brief Get angle between the foot and leg along the leg's +x axis
 * 
 * @param quat_leg      Quaternion from leg to world frame
 * @param quat_foot     Quaternion from foot to world frame
 * @return float 
 */
float getAnkleAngle(const float quat_leg[4], const float quat_foot[4]);

/**
 * @brief Get the ankle velocity along the leg's +x axis
 * 
 * @param quat_leg 
 * @param quat_foot 
 * @param gyro_leg 
 * @param gyro_foot 
 * @return float 
 */
float getAnkleVelocity(const float quat_leg[4], const float quat_foot[4],
                const float gyro_leg[3], const float gyro_foot[3]);

/**
 * Wrapper to turn Serial.write into a callback.
 * 
 * @param data 
 * @param size 
 */
void serialWrite(const byte* data, size_t size);

#endif
