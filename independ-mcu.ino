#include <Wire.h>

#include "src/main.h"


/** Micro-seconds between frames */
const unsigned long TS = 1000000 / 100; // 1e6 / Hz

IMU_LSM9DS1 imu_foot, imu_leg;

// Pointers to imus for quick reference
IMU_LSM9DS1* imus[] = {&imu_foot, &imu_leg};

const float beta = 0.15f;
Madgwick ahrs_foot(1.0e-6f * TS, beta), ahrs_leg(1.0e-6f * TS, beta);

// Struct synchronization tool
Duct duct(serialWrite);

IMUStruct struct_imu;
GaitStruct struct_gait;

const int pin_button = 10;

/**
 * @brief Initialization function
 */
void setup() {

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    // Use internal pullup, so no external resistor is needed
    pinMode(pin_button, INPUT_PULLUP);

    Serial.begin(115200);
    Serial1.begin(115200);

    PRINTLN("Starting...");

    Wire.begin();
    Wire.setClock(400000);

    for (size_t i = 0; i < 2; i++) {

        if (!imus[i]->begin(LSM9DS1_AG_ADDR(i), LSM9DS1_M_ADDR(i)))
        {
            PRINTLN("Failed to communicate with LSM9DS1.");
            PRINTLN("Double-check wiring.");

            while (1) {} // Hang
        }

        imus[i]->setFIFO(FIFO_OFF, 0); // Disable FIFO
        imus[i]->setAccelScale(4); // Max +/- G
        imus[i]->setGyroScale(500);

        delay(100);

        // Initialize AHRS orientation using gravity
        // This will never get the heading (yaw) right, but we don't care about that
        // This works by finding the shortest arc between the current measurement and 
        // gravity when the IMU is upright
        while (!imus[i]->accelAvailable() || !imus[i]->gyroAvailable()) {
            delay(1); // Wait until some acceleration has been measured
        }
        imus[i]->readAccel();
        imus[i]->readGyro();
    }

    delay(50);

    ahrs_foot.quaternionInitIMU(imu_foot.a);
    //ahrs_leg.quaternionInitIMU(imu_leg.a);
    
    // Initialize segments aligned
    ahrs_leg.setQuaternion(ahrs_foot.getQuaternion());

    // Set-up the Duct instance
    struct_imu.acc[0] = struct_imu.acc[1] = struct_imu.acc[2] = 0.0f;
    struct_imu.gyr[0] = struct_imu.gyr[1] = struct_imu.gyr[2] = 0.0f;

    struct_gait.hip_flexion = 0.0f;

    duct.register_struct(struct_imu, sizeof(struct_imu), 0x00);
    duct.register_struct(struct_gait, sizeof(struct_gait), 0x01);
}

/**
 * @brief Main iteration function
 */
void loop() {

    static unsigned long last_frame = 0;  // Keep track of frame time

    // Hang until the previous execution time plus wait time equals a sample time
    bool overflow = true;
    while (micros() - last_frame < TS) {
        overflow = false;
    }

    last_frame = micros();  // Update start time of the next frame

    digitalWrite(LED_BUILTIN, (uint8_t)overflow); // Show LED when loop is overflowed

    main_loop();

    // unsigned long time_taken = micros() - last_frame;

    // PRINT("Time taken: ");
    // PRINTLN(time_taken);
}
