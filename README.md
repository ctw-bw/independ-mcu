# INDEPEND MCU Firmware

This is the micro-controller code for the INDEPEND ankle clutch. The firmware is for Arduino Mega 2560 and the K64F.

More information can be found on [Confluence](https://be.et.utwente.nl/confluence/x/dACAAg).

![Overview Photo](overview.jpg "Overview")

## Versions and Status

This main branch is made for Arduino and can be compiled using Arduino IDE. In the branch `mbed` a version was made based on MBED OS, for the K64F. The K64F has much more processing power and is better equipped for the quaternion processing of IMUs.  
The MBED version can be compiled using MBED Studio or with the MBED Online Compiler.

Both versions are somewhat finished and can compile and run. The code however was never cleaned up. And the interaction with the user to e.g. change settings was also never finished.  
The IMUs are processed to produce a fairly consistent ankle angle. The servo is not yet controlled by this angle.

Data can be live plotted using [uScope](https://bitbucket.org/ctw-bw/uscope).

## Functioning

The two IMUs are each processed with a Madgewick filter to produce an updating quaternion. The problem with a set-up like this, is both quaternions will drift in the heading. Small offsets or noise will quickly built up, resulting in sensors that are drifting in the horizontal plane. To fix this, we simply remove the heading component from both quaternions entirely. This results in a small error when the ankle is rolling, but it successfully eliminates this heading drift.

## Build

You can build the source using Arduino IDE. Simply open the .ino file using Arduino IDE and compile it.

You will need the Sparkfun library for the IMU: https://github.com/sparkfun/SparkFun_LSM9DS1_Arduino_Library  
You can install it directly using the Arduino library manager. Search for "LSM9DS1" and select the latest version.

### VS Code

If you are going to do any development, it is strongly recommended to use VS Code instead of Arduino IDE. VS Code has proper code completion, intellisense, formatting, etc. and makes for a real IDE, unlike the Arduino application.  
Compilation is still done through Arduino IDE, VS Code acts as a front-end.

After installing Arduino IDE and VS Code, get the following VS Code extensions:

 * [Arduino](https://marketplace.visualstudio.com/items?itemName=vsciot-vscode.vscode-arduino)
 * [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
 * [C++ Intellisense](https://marketplace.visualstudio.com/items?itemName=austin.code-gnu-global) (Optional, but recommended)

Then simply open the folder with the .ino file. Use the Command Pallette to build and upload the code.  
For more instructions, see for example <https://maker.pro/arduino/tutorial/how-to-use-visual-studio-code-for-arduino>.

## Code Folders

Arduino IDE can only use .cpp files that are inside the `src/` folder. Therefore all the libraries are installed there too.
